import {Component, OnInit} from '@angular/core';
import {NgNavigatorShareService} from 'ng-navigator-share';

@Component({
  selector: 'app-share-menu',
  templateUrl: './share-menu.component.html',
  styleUrls: ['./share-menu.component.css'],
  providers: [NgNavigatorShareService]
})
export class ShareMenuComponent implements OnInit {

  private ngNavigatorShareService: NgNavigatorShareService;

  constructor(ngNavigatorShareService: NgNavigatorShareService) {
    this.ngNavigatorShareService = ngNavigatorShareService;
  }

  ngOnInit(): void {
  }


  share() {
    this.ngNavigatorShareService.share({
      title: 'My Awesome app',
      text: 'hey check out my Share button',
      url: 'https://developers.google.com/web'
    }).then((response) => {
      console.log(response);
    })
      .catch((error) => {
        console.log(error);
      });
  }
}
