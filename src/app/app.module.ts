import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {NgNavigatorShareService} from 'ng-navigator-share';
import { ShareMenuComponent } from './share-menu/share-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    ShareMenuComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [NgNavigatorShareService],
  bootstrap: [AppComponent],
})
export class AppModule { }
